// models/ballModel.js
const mongoose = require("mongoose");

const Ball = mongoose.model("Ball", {
  name: { type: String },
  value: { type: Number },
  bucket: { type: mongoose.Schema.Types.ObjectId, ref: 'Bucket' },
});

module.exports = Ball;
