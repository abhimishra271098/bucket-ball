// models/bucketModel.js
const mongoose = require("mongoose");

const Bucket = mongoose.model("Bucket", {
  name: { type: String },
  value: { type: Number },
//   numbersOfBall: { type: Number },
  balls: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Ball' }],
});

module.exports = Bucket;
