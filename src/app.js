// controllers/mainController.js
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const Bucket = require('../src/model/bucketModel');
const Ball = require('../src/model/ballModel');
const cors = require('cors');
const path = require('path');


const app = express();
const port = 3001;

mongoose.connect('mongodb+srv://amit_singh:kya_hal_hai_tere@cluster0.jpqo2bq.mongodb.net/bucket_ball_app', { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());
app.set('view engine', 'ejs'); // Set the view engine to EJS
app.set('views', path.join(__dirname, 'views')); // Optional: If you have static files, place them in a 'public' directory

// Define a route to render your EJS file
app.get('/', (req, res) => {
    res.render('index'); // Assuming your EJS file is named 'index.ejs'
});
  

app.post('/addBucket', async (req, res) => {
  const { bucketName, bucketValue } = req.body;
  console.log(`Received data: Bucket Name - ${bucketName}, Value - ${bucketValue}`);
  
  // Assuming Bucket is a mongoose model
  const newBucket = new Bucket({ name: bucketName, value: bucketValue });
  // const newEntry = new User({ username, file: link });
  const user = await newBucket.save();
    res.redirect('/'); // Redirect after successfully saving the bucket
});

app.post('/addBall', async (req, res) => {
  const { ballName, ballValue } = req.body;
  console.log(`Received data: Ball Name - ${ballName}, Value - ${ballValue}`);
  const newBall = new Ball({ name: ballName, value: ballValue});
  const ball = await newBall.save();
  if (ball) {
    res.redirect('/');
  }else{

  }
  // newBall.save((err) => {
  //   if (err) return console.error(err);
  // });
});

app.get('/getBucket', async (req, res) => {
  const ball = await Bucket.find({});
  console.log(`Received data: Ball Name -`,ball);
  if (ball) {
    res.send(ball);
  }else{

  }
});


app.get('/getBall', async (req, res) => {
  const ball = await Ball.find({});
  console.log(`Received data: Ball Name -`,ball);
  if (ball) {
    res.send(ball);
  }else{

  }
});

app.patch('/buckets/:bucketId/addBall/:ballId', async (req, res) => {
  try {
    const { bucketId, ballId } = req.params;

    // Find the bucket by ID
    const bucket = await Bucket.findById(bucketId);

    if (!bucket) {
      return res.status(404).json({ error: "Bucket not found" });
    }

    // Push the ball ID into the balls array
    bucket.balls.push(ballId);

    // Save the updated bucket
    await bucket.save();

    res.json({ message: "Ball added to bucket successfully", bucket });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.get('/details', async (req, res) => {
  // const buckets = await Bucket.find().exec();
  const buckets = await Bucket.find().populate('Ball').exec();
  res.json({ buckets });
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
